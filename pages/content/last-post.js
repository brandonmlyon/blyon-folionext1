import Layout from '../../components/LayoutMain.js'
import Link from 'next/link'
import Safe from "react-safe"
import HeadConfig from '../../components/HeadConfig'
import { NextSeo } from 'next-seo';

export default () => (
    <Layout>

        <HeadConfig />
        <NextSeo
          title="Brandon M Lyon | The End"
          canonical="https://about.brandonmlyon.com/content/last-post"
        />
        <article class="post post22">
        <Safe.style>
        {`
          .post22{--primary-color: #f2c00e;}
        `}
        </Safe.style>
        <div class="articleInnerWrap">
          <div class="box box1of1">

            <h2>You have reached the <em>Last Post</em>.</h2>
            <h4>Thanks for reading</h4>
            <p data-count="343">If you want to read it again you can return to the  <a href="/">most recent post</a>.</p>

          </div>
        </div>
        <a class="pagination__previous" href="/content/how-do-browsers-render-websites"></a>
        </article>
    </Layout>
)
