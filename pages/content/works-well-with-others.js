import Layout from '../../components/LayoutMain.js'
import Link from 'next/link'
import Safe from "react-safe"
import HeadConfig from '../../components/HeadConfig'
import { NextSeo } from 'next-seo';
export default () => (
    <Layout>

        <HeadConfig />
        <NextSeo
          title="Brandon M Lyon | Works well with others"
          canonical="https://about.brandonmlyon.com/content/works-well-with-others"
        />
        <article class="post post11">
        <Safe.style>
        {`
          .post11{--primary-color: #e41b5b;}
        `}
        </Safe.style>
        <div class="articleInnerWrap">
          <div class="box box1of1">

            <h3>Works Well With Others</h3>
            <h5>As a senior developer it's my job to train and mentor new employees. I hold workshops to assist other disciplines. I'm involved in the hiring process and provide questions, tests, and participate in interviews. </h5>
            <h5>As a front-end developer I regularly bridge stakeholders, engineers, designers, and end-users.</h5>
            <h5>As a designer I've managed graphic and industrial design departments responsible for millions of dollars of product.</h5>
            <h5>My experience ranges from a lone wolf responsible for everything to large teams with &gt; 100 people, both in person and remotely.</h5>

          </div>
        </div>
        <a class="pagination__previous" href="/content/natural-language-question-closing-process"></a>
        <a class="pagination__next" href="/content/marketo"></a>
        <Safe.script>{`
            updateNextURL(document);
        `}</Safe.script>
        </article>
    </Layout>
)
