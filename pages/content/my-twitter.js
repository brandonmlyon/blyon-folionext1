import Layout from '../../components/LayoutMain.js'
import Link from 'next/link'
import Safe from "react-safe"
import HeadConfig from '../../components/HeadConfig'
import { NextSeo } from 'next-seo';

export default () => (
    <Layout>

        <HeadConfig />
        <NextSeo
          title="Brandon M Lyon | My Twitter"
          canonical="https://about.brandonmlyon.com/content/my-twitter"
        />
        <article class="post post17">
        <Safe.style>
        {`
          .post17{--primary-color: #1da1f2;}
        `}
        </Safe.style>
        <div class="articleInnerWrap">
          <div class="box box1of1">

            <h2>My <em>Twitter</em></h2>
            <h6>Industry news</h6>
            <h5><blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Things I wish I knew about decades ago: Bracket Pair Colorizer! <a href="https://t.co/6fdAVQF52m">https://t.co/6fdAVQF52m</a></p>&mdash; BrandonMLyon (@brandon_m_lyon) <a href="https://twitter.com/brandon_m_lyon/status/1108440799010676736?ref_src=twsrc%5Etfw">March 20, 2019</a></blockquote>
            <Safe.script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></Safe.script>
            </h5>

          </div>
        </div>
        <a class="pagination__previous" href="/content/apparel-e-commerce"></a>
        <a class="pagination__next" href="/content/re-brand-and-re-build-from-scratch"></a>
        <Safe.script>{`
            updateNextURL(document);
        `}</Safe.script>
        </article>
    </Layout>
)
