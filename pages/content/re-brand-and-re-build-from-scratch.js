import Layout from '../../components/LayoutMain.js'
import Link from 'next/link'
import Safe from "react-safe"
import HeadConfig from '../../components/HeadConfig'
import { NextSeo } from 'next-seo';

export default () => (
    <Layout>

        <HeadConfig />
        <NextSeo
          title="Brandon M Lyon | Re-brands"
          canonical="https://about.brandonmlyon.com/content/re-brand-and-re-build-from-scratch"
        />
        <article class="post post18">
        <Safe.style>
        {`
          .post18{--primary-color: #3f4f70;}
        `}
        </Safe.style>
        <div class="articleInnerWrap">
          <div class="box box1of2">

            <h4>Re-brand and Re-build <em>From Scratch</em></h4>
            <h2>Hundreds of pages in a couple of months</h2>
            <h6>Design and development for Experts Exchange</h6>
            <p data-count="343">Experts Exchange has a massive codebase and hundreds of page types. Someone has to plan it all. I've planned the front-end infrastructure and codebase re-organization multiple times since 2009. It took three of us a couple of months of long nights working to re-build the front-end of the entire site from scratch.</p>

          </div>
          <div class="box box2of2">
            <Safe.style>
              {`.post18 .imageContainer {background-image:url("/static/images/ee-home.gif");}`}
            </Safe.style>
            <div class="imageContainer"></div>
          </div>
        </div>
        <a class="pagination__previous" href="/content/my-twitter"></a>
        <a class="pagination__next" href="/content/my-pinterest"></a>
        <Safe.script>{`
            updateNextURL(document);
        `}</Safe.script>
        </article>
    </Layout>
)
