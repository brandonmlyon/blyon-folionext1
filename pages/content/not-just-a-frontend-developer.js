import Layout from '../../components/LayoutMain.js'
import Link from 'next/link'
import Safe from "react-safe"
import HeadConfig from '../../components/HeadConfig'
import { NextSeo } from 'next-seo';

export default () => (
    <Layout>

        <HeadConfig />
        <NextSeo
          title="Brandon M Lyon | Not just a front-end developer"
          canonical="https://about.brandonmlyon.com/content/not-just-a-frontend-developer"
        />
        <article class="post post3">
        <Safe.style>
        {`
          .post3{--primary-color: #f2c00e;}
        `}
        </Safe.style>
        <div class="articleInnerWrap">
          <div class="box box1of1">

            <h3>Not Just A Front-End Developer</h3>
            <h5>Front-end software is my specialty but I've done full stack development including devops and a little bit of soldering. Websites are my passion but I also design physical things for multiple industries. Graphic design, apparel design, industrial design, architectural design, and photography.</h5>

          </div>
        </div>
        <a class="pagination__previous" href="/content/navigation-revamp-and-new-dashboards"></a>
        <a class="pagination__next" href="/content/question-page-revamp"></a>
        <Safe.script>{`
            updateNextURL(document);
        `}</Safe.script>
        </article>
    </Layout>
)
