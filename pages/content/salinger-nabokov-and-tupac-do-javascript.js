import Layout from '../../components/LayoutMain.js'
import Link from 'next/link'
import Safe from "react-safe"
import HeadConfig from '../../components/HeadConfig'
import { NextSeo } from 'next-seo';

export default () => (
    <Layout>

        <HeadConfig />
        <NextSeo
          title="Brandon M Lyon | Salinger, Nabakov, and Tupac do Javascript"
          canonical="https://about.brandonmlyon.com/content/salinger-nabokov-and-tupac-do-javascript"
        />
        <article class="post post13">
        <Safe.style>
        {`
          .post13{--primary-color: #f2c00e;}
        `}
        </Safe.style>
        <div class="articleInnerWrap">
          <div class="box box1of1">

            <h6>Recommended Reading</h6>
            <h5>"Follow your heart, but take JavaScript with you."</h5>
            <h3><a href="http://blog.anguscroll.com/tupac-does-javascript" target="_blank">🔗 Salinger, Nabokov and Tupac do JavaScript</a></h3>

          </div>
        </div>
        <a class="pagination__previous" href="/content/marketo"></a>
        <a class="pagination__next" href="/content/content-management-systems-and-blogs"></a>
        <Safe.script>{`
            updateNextURL(document);
        `}</Safe.script>
        </article>
    </Layout>
)
