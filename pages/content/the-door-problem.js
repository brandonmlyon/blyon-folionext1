import Layout from '../../components/LayoutMain.js'
import Link from 'next/link'
import Safe from "react-safe"
import HeadConfig from '../../components/HeadConfig'
import { NextSeo } from 'next-seo';

export default () => (
    <Layout>

        <HeadConfig />
        <NextSeo
          title="Brandon M Lyon | The door problem"
          canonical="https://about.brandonmlyon.com/content/the-door-problem"
        />
        <article class="post post5">
        <Safe.style>
        {`
          .post5{--primary-color: #00a3e0;}
        `}
        </Safe.style>
        <div class="articleInnerWrap">
          <div class="box box1of1">

            <h6>Recommended Reading</h6>
            <h5>"Design is a nebulous term to most people. I like to describe the "The Door Problem." Someone has to specify how doors work. It's not as obvious as you might think."</h5>
            <h3><a href="http://www.lizengland.com/blog/2014/04/the-door-problem/" target="_blank">🔗 The Door Problem</a></h3>

          </div>
        </div>
        <a class="pagination__previous" href="/content/question-page-revamp"></a>
        <a class="pagination__next" href="/content/mobile-app-development"></a>
        <Safe.script>{`
            updateNextURL(document);
        `}</Safe.script>
        </article>
    </Layout>
)
