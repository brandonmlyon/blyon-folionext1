import Layout from '../../components/LayoutMain.js'
import Link from 'next/link'
import Safe from "react-safe"
import HeadConfig from '../../components/HeadConfig'
import { NextSeo } from 'next-seo';

export default () => (
    <Layout>

        <HeadConfig />
        <NextSeo
          title="Brandon M Lyon | Marketo"
          canonical="https://about.brandonmlyon.com/content/marketo"
        />
        <article class="post post12">
        <Safe.style>
        {`
          .post12{--primary-color: #5a54a4;}
        `}
        </Safe.style>
        <div class="articleInnerWrap">
          <div class="box box1of2">

            <h4><em>Marketo</em></h4>
            <h2>Landing pages, forms, drip campaigns, & lead funnels</h2>
            <h6>Development for Experts Exchange</h6>
            <p data-count="343">Marketo is an important tool for many marketers but it's not intuitive for a developer to work with. Over the years I've helped marketers develop their Marketo campaigns using modern front-end development techniques.</p>

          </div>
          <div class="box box2of2">
            <Safe.style>
              {`.post12 .imageContainer {background-image:url("/static/images/ee-marketo.jpg");}`}
            </Safe.style>
            <div class="imageContainer"></div>
          </div>
        </div>
        <a class="pagination__previous" href="/content/works-well-with-others"></a>
        <a class="pagination__next" href="/content/salinger-nabokov-and-tupac-do-javascript"></a>
        <Safe.script>{`
            updateNextURL(document);
        `}</Safe.script>
        </article>
    </Layout>
)
