import Layout from '../../components/LayoutMain.js'
import Link from 'next/link'
import Safe from "react-safe"
import HeadConfig from '../../components/HeadConfig'
import { NextSeo } from 'next-seo';

export default () => (
    <Layout>

        <HeadConfig />
        <NextSeo
          title="Brandon M Lyon | Closing process"
          canonical="https://about.brandonmlyon.com/content/natural-language-question-closing-process"
        />
        <article class="post post10">
        <Safe.style>
        {`
          .post10{--primary-color: #3f4f70;}
        `}
        </Safe.style>
        <div class="articleInnerWrap">
          <div class="box box1of2">

            <h4><em>Natural Language</em> Question Closing Process</h4>
            <h2>Core functionality for a Q&amp;A website</h2>
            <h6>UX and Development for Experts Exchange</h6>
            <p data-count="343">We needed to increase question closure, gain metrics for solver performance, and reduce solver infighting. We switched from one-word CTAs like "answer" to phrases like "is this your solution?" For metrics we added helpful &amp; unhelpful buttons. To reduce infighting we allowed more than one solution and made it so everyone can choose their own solutions.</p>

          </div>
          <div class="box box2of2">
            <Safe.style>
              {`.post10 .imageContainer {background-image:url("/static/images/ee-closing.jpg");}`}
            </Safe.style>
            <div class="imageContainer"></div>
          </div>
        </div>
        <a class="pagination__previous" href="/content/about-me"></a>
        <a class="pagination__next" href="/content/works-well-with-others"></a>
        <Safe.script>{`
            updateNextURL(document);
        `}</Safe.script>
        </article>
    </Layout>
)
