import Layout from '../../components/LayoutMain.js'
import Link from 'next/link'
import Safe from "react-safe"
import HeadConfig from '../../components/HeadConfig'
import { NextSeo } from 'next-seo';

export default () => (
    <Layout>

        <HeadConfig />
        <NextSeo
          title="Brandon M Lyon | E-commerce"
          canonical="https://about.brandonmlyon.com/content/apparel-e-commerce"
        />
        <article class="post post16">
        <Safe.style>
        {`
          .post16{--primary-color: #444444;}
        `}
        </Safe.style>
        <div class="articleInnerWrap">
          <div class="box box1of2">

            <h4>Apparel <em>E-commerce</em></h4>
            <h2>Clients with discerning tastes</h2>
            <h6>Design and development for Straightdown</h6>
            <p data-count="343">Straightdown had a ten year old and discontinued e-commerce application. I rebuilt it alone and improved it using the Magento platform. There were hundreds of distinct apparel products with iterations of size, cut, material, and color. Sales and coupons were a regular occurrence.  The site had to integrate with their shipping database. I also built and integrated a Wordpress blog for their product news.</p>

          </div>
          <div class="box box2of2">
            <Safe.style>
              {`.post16 .imageContainer {background-image:url("/static/images/sd-home.jpg");}`}
            </Safe.style>
            <div class="imageContainer"></div>
          </div>
        </div>
        <a class="pagination__previous" href="/content/a-software-tester-walks-into-a-bar"></a>
        <a class="pagination__next" href="/content/my-twitter"></a>
        <Safe.script>{`
            updateNextURL(document);
        `}</Safe.script>
        </article>
    </Layout>
)
