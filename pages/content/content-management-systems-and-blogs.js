import Layout from '../../components/LayoutMain.js'
import Link from 'next/link'
import Safe from "react-safe"
import HeadConfig from '../../components/HeadConfig'
import { NextSeo } from 'next-seo';

export default () => (
    <Layout>

        <HeadConfig />
        <NextSeo
          title="Brandon M Lyon | CMS Content Management Systems and Blogs"
          canonical="https://about.brandonmlyon.com/content/content-management-systems-and-blogs"
        />
        <article class="post post14">
        <Safe.style>
        {`
          .post14{--primary-color: #00a3e0;}
        `}
        </Safe.style>
        <div class="articleInnerWrap">
          <div class="box box1of2">

            <h4><em>C</em>ontent <em>M</em>anagement <em>S</em>ystems and Blogs</h4>
            <h2>Custom functions, themes, plugins, SEO, and security</h2>
            <h6>Development for Experts Exchange</h6>
            <p data-count="343">When you run a community based website with user generated content it's important to connect with your users. I've built and rebuilt this blog several times from scratch. Always optimize the performance, security, and SEO of content management systems such as Wordpress.</p>

          </div>
          <div class="box box2of2">
            <Safe.style>
              {`.post14 .imageContainer {background-image:url("/static/images/ee-blog.jpg");}`}
            </Safe.style>
            <div class="imageContainer"></div>
          </div>
        </div>
        <a class="pagination__previous" href="/content/salinger-nabokov-and-tupac-do-javascript"></a>
        <a class="pagination__next" href="/content/a-software-tester-walks-into-a-bar"></a>
        <Safe.script>{`
            updateNextURL(document);
        `}</Safe.script>
        </article>
    </Layout>
)
