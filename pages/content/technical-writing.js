import Layout from '../../components/LayoutMain.js'
import Link from 'next/link'
import Safe from "react-safe"
import HeadConfig from '../../components/HeadConfig'

import { NextSeo } from 'next-seo';
export default () => (
    <Layout>

        <HeadConfig />
        <NextSeo
          title="Brandon M Lyon | Technical writing"
          canonical="https://about.brandonmlyon.com/content/technical-writing"
        />
        <article class="post post20">
        <Safe.style>
        {`
          .post20{--primary-color: #3f4f70;}
        `}
        </Safe.style>
        <div class="articleInnerWrap">
          <div class="box box1of2">

            <h4>Technical <em>Writing</em></h4>
            <h2>Blogging, technical documents, whitepapers, and slideshares</h2>
            <h6>Writing and design for Experts Exchange, XSense, and Straightdown</h6>
            <p data-count="343">Good developers need to write well for more than just developer documentation. I've prepared professional research papers, brochureware, whitepapers, and slideshares. Over the years I've contributed many articles to assist my employers with content creation.</p>

          </div>
          <div class="box box1of2">
          <Safe.style>
          {`
            .post20 li {
              font-size: 1.6rem;
              line-height: 1.5;
            }
          `}
          </Safe.style>
            <ul>
              <li><a href="https://about.gitlab.com/blog/2019/11/06/how-to-stay-productive-in-your-home-office/" target="_blank">How To Stay Productive Working Remotely</a></li>
              <li><a href="http://blog.experts-exchange.com/ee-blog/css-best-practices/" target="_blank">CSS Best Practices</a></li>
              <li><a href="https://www.experts-exchange.com/articles/30601/Code-Responsibly.html" target="_blank">Code Responsibly</a></li>
              <li><a href="http://blog.experts-exchange.com/ee-blog/top-tech-gifts-for-any-budget/" target="_blank">Top Tech Gifts For Any Budget</a></li>
              <li><a href="https://www.experts-exchange.com/articles/13925/How-to-Sync-Zimbra-Calendars-with-Android.html" target="_blank">How To Sync Zimbra Calendars With Android</a></li>
              <li><a href="https://www.experts-exchange.com/articles/18019/Before-you-decide-to-run-your-own-webserver.html" target="_blank">Before You Decide To Run Your Own Webserver</a></li>
              <li><a href="https://www.experts-exchange.com/articles/26440/Create-a-free-website-using-git-and-polymer.html" target="_blank">Create A Free Website Using Git &amp; Polymer</a></li>
              <li><a href="https://www.experts-exchange.com/articles/19121/Life-with-an-Amazon-Echo.html" target="_blank">Life With An Amazon Echo</a></li>
              <li><a href="https://www.experts-exchange.com/articles/31172/How-to-Follow-Instagram-Users-or-Hashtags-via-RSS.html" target="_blank">How To Follow Instagram Users Or Hashtags Via RSS</a></li>
              <li><a href="https://www.experts-exchange.com/articles/28608/Mobile-App-Development-Best-Practices.html" target="_blank">Mobile App Development Best Practices</a></li>
              <li><a href="http://blog.experts-exchange.com/ee-blog/get-started-with-javascript/" target="_blank">Get Started With Javascript</a></li>
            </ul>
          </div>
        </div>
        <a class="pagination__previous" href="/content/my-pinterest"></a>
        <a class="pagination__next" href="/content/how-do-browsers-render-websites"></a>
        <Safe.script>{`
            updateNextURL(document);
        `}</Safe.script>
        </article>
    </Layout>
)
