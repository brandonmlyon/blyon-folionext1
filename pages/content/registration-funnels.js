import Layout from '../../components/LayoutMain.js'
import Link from 'next/link'
import Safe from "react-safe"
import HeadConfig from '../../components/HeadConfig'
import { NextSeo } from 'next-seo';

export default () => (
    <Layout>

        <HeadConfig />
        <NextSeo
          title="Brandon M Lyon | Registration funnels"
          canonical="https://about.brandonmlyon.com/content/registration-funnels"
        />
        <article class="post post8">
        <Safe.style>
        {`
          .post8{--primary-color: #00a3e0;}
        `}
        </Safe.style>
        <div class="articleInnerWrap">
          <div class="box box1of2">

            <h4><em>Registration</em> Funnels</h4>
            <h2>The Money-Maker</h2>
            <h6>Development for Experts Exchange</h6>
            <p data-count="343">If you are a subscription based company it is very important to have well built payment funnels. Ones with coupon codes, foreign currency, account credits, GDPR, feature bundles, account tiers, subscription durations, friendly and specific error messages, and more. Experts Exchange trusted me to build these funnels.</p>

          </div>
          <div class="box box2of2">
            <Safe.style>
              {`.post8 .imageContainer {background-image:url("/static/images/ee-register2018q4.jpg");}`}
            </Safe.style>
            <div class="imageContainer"></div>
          </div>
        </div>
        <a class="pagination__previous" href="/content/about-this-website"></a>
        <a class="pagination__next" href="/content/about-me"></a>
        <Safe.script>{`
            updateNextURL(document);
        `}</Safe.script>
        </article>
    </Layout>
)
