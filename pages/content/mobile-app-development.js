import Layout from '../../components/LayoutMain.js'
import Link from 'next/link'
import Safe from "react-safe"
import HeadConfig from '../../components/HeadConfig'
import { NextSeo } from 'next-seo';

export default () => (
    <Layout>

        <HeadConfig />
        <NextSeo
          title="Brandon M Lyon | Mobile app development"
          canonical="https://about.brandonmlyon.com/content/mobile-app-development"
        />
        <article class="post post6">
        <Safe.style>
        {`
          .post6{--primary-color: #3f4f70;}
        `}
        </Safe.style>
        <div class="articleInnerWrap">
          <div class="box box1of2">

            <h4>Mobile <em>App</em> Development</h4>
            <h2>Always-connected access for our most important users</h2>
            <h6>UI and development for Experts Exchange</h6>
            <p data-count="343">Using Appcelerator and then rebuilding with Ionic ( Angular ), I have worked on a team of 2 developing 3 major revisions of the Experts Exchange mobile app. This involved research into common UI patterns and the app store submission process. Due to strictly following Apple's UI guidelines we passed on the first submission.</p>

          </div>
          <div class="box box2of2">
            <Safe.style>
              {`.post6 .imageContainer {background-image:url("/static/images/ee-mobileapp2019q2.jpg");}`}
            </Safe.style>
            <div class="imageContainer"></div>
          </div>
        </div>
        <a class="pagination__previous" href="/content/the-door-problem"></a>
        <a class="pagination__next" href="/content/about-this-website"></a>
        <Safe.script>{`
            updateNextURL(document);
        `}</Safe.script>
        </article>
    </Layout>
)
