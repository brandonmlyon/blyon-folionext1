import Layout from '../../components/LayoutMain.js'
import Link from 'next/link'
import Safe from "react-safe"
import HeadConfig from '../../components/HeadConfig'
import { NextSeo } from 'next-seo';

export default () => (
    <Layout>

        <HeadConfig />
        <NextSeo
          title="Brandon M Lyon | Header Revamp"
          canonical="https://about.brandonmlyon.com/content/navigation-revamp-and-new-dashboards"
        />
        <article class="post post2">
        <Safe.style>
        {`
          .post2{--primary-color: #00a3e0;}
        `}
        </Safe.style>
        <div class="articleInnerWrap">
          <div class="box box1of2">

            <h4>Navigation <em>Revamp</em> and New Dashboards</h4>
            <h2>Core business functionality impacting millions of members</h2>
            <h6>UX & development for Experts Exchange</h6>
            <p>The previous header had too many items people didn't interact with regularly. Users couldn't find what they needed. Based on gathered metrics, user research, and F.I.T. principles, we implemented multiple new header variations with different links for different user types. In order to reduce complexity and increase functionality, less important items were moved out of the header and into new dashboards. Certain actions on site corrolate with increased subscriber retention so we prioritized those actions. These combined goals all resulted in positive metrics trends.</p>

          </div>
          <div class="box box2of2">
            <Safe.style>
              {`.post2 .imageContainer {background-image:url("/static/images/ee-header2019q1.jpg");}`}
            </Safe.style>
            <div class="imageContainer"></div>
          </div>
        </div>
        <a class="pagination__previous" href="/"></a>
        <a class="pagination__next" href="/content/not-just-a-frontend-developer"></a>
        <Safe.script>{`
            updateNextURL(document);
        `}</Safe.script>
        </article>
    </Layout>
)
