import Layout from '../../components/LayoutMain.js'
import Link from 'next/link'
import Safe from "react-safe"
import HeadConfig from '../../components/HeadConfig'
import { NextSeo } from 'next-seo';

export default () => (
    <Layout>

        <HeadConfig />
        <NextSeo
          title="Brandon M Lyon | A software tester walks into a bar"
          canonical="https://about.brandonmlyon.com/content/a-software-tester-walks-into-a-bar"
        />
        <article class="post post15">
        <Safe.style>
        {`
          .post15{--primary-color: #3f4f70;}
        `}
        </Safe.style>
        <div class="articleInnerWrap">
          <div class="box box1of1">

            <h3><a href="https://www.sempf.net/post/On-Testing1" target="_blank">🔗 A software tester walks into a bar</a></h3>
            <h6>Recommended Reading</h6>
            <h5>"Orders a beer. Orders 0 beers. Orders 999999999 beers. Orders a lizard. Orders -1 beers. Orders a sfdeljknesv."</h5>

          </div>
        </div>
        <a class="pagination__previous" href="/content/content-management-systems-and-blogs"></a>
        <a class="pagination__next" href="/content/apparel-e-commerce"></a>
        <Safe.script>{`
            updateNextURL(document);
        `}</Safe.script>
        </article>
    </Layout>
)
