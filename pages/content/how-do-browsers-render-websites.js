import Layout from '../../components/LayoutMain.js'
import Link from 'next/link'
import Safe from "react-safe"
import HeadConfig from '../../components/HeadConfig'
import { NextSeo } from 'next-seo';

export default () => (
    <Layout>

        <HeadConfig />
        <NextSeo
          title="Brandon M Lyon | How do browsers render websites"
          canonical="https://about.brandonmlyon.com/content/how-do-browsers-render-websites"
        />
        <article class="post post21">
        <Safe.style>
        {`
          .post21{--primary-color: #444444;}
        `}
        </Safe.style>
        <div class="articleInnerWrap">
          <div class="box box1of1">

            <h2><em>How Do Browsers Render Websites</em></h2>
            <h4>Things every front-ender should know</h4>
            <h6>&nbsp;</h6>
            <div class="videoContainer">
               <iframe width="1280" height="720" src="https://www.youtube.com/embed/SmE4OwHztCc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>

          </div>
        </div>
        <a class="pagination__previous" href="/content/technical-writing"></a>
        <a class="pagination__next" href="/content/last-post"></a>
        <Safe.script>{`
            updateNextURL(document);
        `}</Safe.script>
        </article>
    </Layout>
)
