import Layout from '../../components/LayoutMain.js'
import Link from 'next/link'
import Safe from "react-safe"
import HeadConfig from '../../components/HeadConfig'
import { NextSeo } from 'next-seo';

export default () => (
    <Layout>

        <HeadConfig />
        <NextSeo
          title="Brandon M Lyon | About Me"
          canonical="https://about.brandonmlyon.com/content/about-me"
        />
        <article class="post post9">
        <Safe.style>
        {`
          .post9{--primary-color: #f2c00e;}
        `}
        </Safe.style>
        <div class="articleInnerWrap">
          <div class="box box1of1">

            <h3>About Me</h3>
            <h5>As a person with an eye for detail the design profession seemed like a perfect match for me from an early age. I love to research, plan, and create. I can't imagine doing anything else with my life. I enjoy discussing modern design trends, learning new techniques, and figuring out how things work and are made. Reading design and technology news on a daily basis is an important part of my life. If I had to pick a few of my favorite things other than a computer, they would be CMYK, frisbees, volleyball, cars, music, architecture, and video games.</h5>

          </div>
        </div>
        <a class="pagination__previous" href="/content/registration-funnels"></a>
        <a class="pagination__next" href="/content/natural-language-question-closing-process"></a>
        <Safe.script>{`
            updateNextURL(document);
        `}</Safe.script>
        </article>
    </Layout>
)
