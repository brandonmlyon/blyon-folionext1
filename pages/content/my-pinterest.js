import Layout from '../../components/LayoutMain.js'
import Link from 'next/link'
import Safe from "react-safe"
import HeadConfig from '../../components/HeadConfig'
import { NextSeo } from 'next-seo';

export default () => (
    <Layout>

        <HeadConfig />
        <NextSeo
          title="Brandon M Lyon | My Pinterest"
          canonical="https://about.brandonmlyon.com/content/my-pinterest"
        />
        <article class="post post19">
        <Safe.style>
        {`
          .post19{--primary-color: #e41b5b;}
          .post19 .box1of1 {max-width: 1380px;}

          .moodboards .listItem {
            display: inline-block;
            height: 430px;
            margin: 0 2em 2em 0;
            width: 265px;
          }
        `}
        </Safe.style>
        <div class="articleInnerWrap">
          <div class="box box1of1">

            <h2>My <em>Pinterest</em></h2>
            <div class="moodboards">
        <div class="listItem">
          <a data-pin-board-width="400" data-pin-do="embedBoard" data-pin-scale-height="320" data-pin-scale-width="80" href="https://www.pinterest.com/designbybrandon/website-design-inspiration/"></a>
        </div>
        <div class="listItem">
          <a data-pin-board-width="400" data-pin-do="embedBoard" data-pin-scale-height="320" data-pin-scale-width="80" href="https://www.pinterest.com/designbybrandon/mobile-design-inspiration/"></a>
        </div>
        <div class="listItem">
          <a data-pin-board-width="400" data-pin-do="embedBoard" data-pin-scale-height="320" data-pin-scale-width="80" href="https://www.pinterest.com/designbybrandon/graphic-design-inspiration/"></a>
        </div>
        <div class="listItem">
          <a data-pin-board-width="400" data-pin-do="embedBoard" data-pin-scale-height="320" data-pin-scale-width="80" href="https://www.pinterest.com/designbybrandon/colorways/"></a>
        </div>
      </div>
      <Safe.script>
        {`
         var showPins = function() {
            var script = document.createElement('script');
            var prior = document.getElementsByTagName('script')[0];
            script.async = 1;
            prior.parentNode.insertBefore(script, prior);
            script.src = 'https://assets.pinterest.com/js/pinit_main.js';
         };
         showPins();
        `}
        </Safe.script>
          </div>
        </div>
        <a class="pagination__previous" href="/content/re-brand-and-re-build-from-scratch"></a>
        <a class="pagination__next" href="/content/technical-writing"></a>
        <Safe.script>{`
            updateNextURL(document);
        `}</Safe.script>
        </article>
    </Layout>
)
