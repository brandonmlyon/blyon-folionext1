import Layout from '../../components/LayoutMain.js'
import Link from 'next/link'
import Safe from "react-safe"
import HeadConfig from '../../components/HeadConfig'
import { NextSeo } from 'next-seo';

export default () => (
    <Layout>

        <HeadConfig />
        <NextSeo
          title="Brandon M Lyon | Human centered design"
          canonical="https://about.brandonmlyon.com/content/question-page-revamp"
        />
        <article class="post post4">
        <Safe.style>
        {`
          .post4{--primary-color: #444444;}
        `}
        </Safe.style>
        <div class="articleInnerWrap">
          <div class="box box1of2">

            <h4>Question Page Revamp</h4>
            <h2>A successful redesign of the most important page</h2>
            <h6>UX & development for Experts Exchange.</h6>
            <p data-count="343">This is the first impression for new users where > 80% of logged-out traffic occurs. Because of my attention to detail I'm repeatedly trusted with it's development. This was my first chance to design the page. I increased clickthroughs for the primary call-to-action by reducing distractions and updating the page to a single column design.</p>

          </div>
          <div class="box box2of2">
            <Safe.style>
              {`.post4 .imageContainer {background-image:url("/static/images/ee-vqp2018q3.jpg");}`}
            </Safe.style>
            <div class="imageContainer"></div>
          </div>
        </div>
        <a class="pagination__previous" href="/content/not-just-a-frontend-developer"></a>
        <a class="pagination__next" href="/content/the-door-problem"></a>
        <Safe.script>{`
            updateNextURL(document);
        `}</Safe.script>
        </article>
    </Layout>
)
