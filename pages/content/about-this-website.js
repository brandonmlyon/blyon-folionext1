import Layout from '../../components/LayoutMain.js'
import Link from 'next/link'
import Safe from "react-safe"
import HeadConfig from '../../components/HeadConfig'
import { NextSeo } from 'next-seo';

export default () => (
    <Layout>

        <HeadConfig />
        <NextSeo
          title="Brandon M Lyon | About this website"
          canonical="https://about.brandonmlyon.com/content/about-this-website"
        />
        <article class="post post7">
        <Safe.style>
        {`
          .post7{--primary-color: #e41b5b;}
        `}
        </Safe.style>
        <div class="articleInnerWrap">
          <div class="box box1of1">

            <h3>About this website</h3>
            <h5>This website was created by hand in very little time using minimalist theories. It's a Firebase hosted <a href="https://nextjs.org/" target="_blank">Next.js</a> (React) based site. The project was once a VPS hosted <a href="https://www.meteor.com/" target="_blank">Meteor</a> site. Then I turned it into a git hosted <a href="https://www.polymer-project.org/" target="_blank">Polymer</a> project. That was followed by a Firebase deployed <a href="https://riot.js.org/" target="_blank">Riot.js</a> app. I plan to rebuild it using <a href="https://svelte.dev/" target="_blank">svelte.js</a>, possibly on Netlify.</h5>

          </div>
        </div>
        <a class="pagination__previous" href="/content/mobile-app-development"></a>
        <a class="pagination__next" href="/content/registration-funnels"></a>
        <Safe.script>{`
            updateNextURL(document);
        `}</Safe.script>
        </article>
    </Layout>
)
