import Layout from '../components/LayoutMain.js'
import Link from 'next/link'
import Safe from "react-safe"
import HeadConfig from '../components/HeadConfig'
import { NextSeo } from 'next-seo';

// Initial page must be at least 1px taller than viewport to trigger scrollbar and infinite scroll

export default () => (
    <Layout>

        <HeadConfig />
        <NextSeo
          title="Brandon M Lyon is a developer and designer with 15 years experience."
          description="I'm a full stack developer specializing in front-end. Websites are my passion but I also do graphic design, apparel, industrial, architectural, and photography."
          canonical="https://about.brandonmlyon.com/"
        />
        <article style={{minHeight: 'calc(100vh - 64px + 1px)'}} class="post post1">
        <Safe.style>
        {`
          .post1{--primary-color: #e41b5b;}
        `}
        </Safe.style>
        <div class="articleInnerWrap">
          <div class="box box1of1">

            <h3>Brandon M Lyon is a professional front-end developer and designer with over 15 years of industry experience.</h3>
            <h5>As you <strong>scroll through this site</strong> I'll be sharing projects I've worked on, personal factoids, testimonials, industry news, techniques, code snippets, pins, tweets, videos, and more.</h5>

          </div>
        </div>
        <a class="pagination__next" href="/content/navigation-revamp-and-new-dashboards"></a>
        </article>
    </Layout>
)
