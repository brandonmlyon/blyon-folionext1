import Layout from '../components/LayoutMain.js'
import Link from 'next/link'
import Safe from "react-safe"
import HeadConfig from '../components/HeadConfig'
import { NextSeo } from 'next-seo';

export default () => (
    <Layout>

        <HeadConfig title="BML | Contact" />
        <NextSeo
          title="Brandon M Lyon | Contact"
          canonical="https://about.brandonmlyon.com/contact"
        />
        <article class="post contactPage">
        <Safe.style>
        {`
          .contactPage{--primary-color: #e41b5b;}
          .contactPage .box1of1 li {
            font-size: 1.8rem;
            line-height: 1.75;
            list-style: none;
            text-align: center;
          }
        `}
        </Safe.style>
        <div class="articleInnerWrap">
          <div class="box box1of1">

            <ul>
                <li>Brandon Lyon</li>
                <li>805-689-0077</li>
                <li><a href='mailt&#111;&#58;%62m%6&#67;y%6Fn&#64;g%6&#68;%61&#105;l&#46;%63%6Fm'>&#98;m&#108;yon&#64;&#103;m&#97;&#105;l&#46;&#99;&#111;m</a></li>
                <li><a href="/static/bmlyon_resume.pdf">Resume</a></li>
                <li><a href="https://www.linkedin.com/in/brandonmlyon/">LinkedIn</a></li>
                <li><a href="https://twitter.com/brandon_m_lyon">Twitter</a></li>
            </ul>

          </div>
        </div>
        </article>
    </Layout>
)
