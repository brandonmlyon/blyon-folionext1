import Head from 'next/head'
export default () => (
  <div>
    <Head>
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
      <meta charSet="utf-8" />
      <meta name="p:domain_verify" content="cb3e264d1fd54b829f4c58f797f2ad57"/>
      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-169750670-1"></script>
      <script
          dangerouslySetInnerHTML={{
            __html: `window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}gtag('js', new Date());gtag('config', 'UA-169750670-1');`,
          }}
      ></script>
    </Head>
  </div>
)
