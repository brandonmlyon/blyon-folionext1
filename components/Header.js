import Link from 'next/link'
import Css from '../static/css/components/Header.css'

const Header = () => (
    <div class="headerWrapper">
      <header>
        <nav>
            <a href="/" title="Home">Brandon M Lyon</a>
            <span>|</span>
            <a href="/contact" class="headerCtaPrimary">Contact</a>
        </nav>
      </header>
    </div>
)

export default Header
