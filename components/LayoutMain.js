import '../static/css/components/Normalize.css'
import '../static/css/components/FontStack.css'
import '../static/css/components/LayoutMain.css'
import Header from './Header'

const Layout = (props) => (
  <div>
    <Header />
    <main class="container">
    {props.children}
    </main>
    <script src="../static/js/infinitescroll.js"></script>
  </div>
)

export default Layout
